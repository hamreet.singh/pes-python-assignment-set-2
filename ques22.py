import math

x = float(input('Enter the value of x (0.1 - 0.9) : '))
y = float(input('Enter the value of y (0.1 - 0.9) : '))

print(f'sin value of {x} : ', math.sin(x))
print(f'cos value of {x} : ', math.cos(x))
print(f'tan value of {x} : ', math.tan(x))

print(f'Arc cosine of {x} : ', math.acos(x))
print(f'Arc sine of {x}: ', math.asin(x))
print(f'Arc tangent of {x}: ', math.atan(x))
print(f'Arc tangent {x}/{y} of : ', math.atan2(y, x))

print(f'Hyperbolic Sine of {y} : ', math.sinh(y))
print(f'Hyperbolic Cosine of {y} : ', math.cosh(y))
print(f'Hyperbolic Tangent of {y} : ', math.tanh(y))

print(f'Inverse Hyperbolic Sine of {y} : ', math.asinh(y))
print(f'Inverse Hyperbolic Cosine of {y} : ', math.acosh(math.degrees(y)))
print(f'Inverse Hyperbolic Tangent of {y} : ', math.atanh(y))
