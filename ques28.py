string = input("Enter a string: ")

a_counter = 0
e_counter = 0
i_counter = 0
o_counter = 0
u_counter = 0

for i in string:
    if i.lower() == 'a':
        a_counter += 1
    elif i.lower() == 'e':
        e_counter += 1
    elif i.lower() == 'i':
        i_counter += 1
    elif i.lower() == 'o':
        o_counter += 1
    elif i.lower() == 'u':
        u_counter += 1

print("total number of vowels in the string are: {}".format(a_counter + e_counter + i_counter + o_counter + u_counter))
print("number of 'a': {}".format(a_counter))
print("number of 'e': {}".format(e_counter))
print("number of 'i': {}".format(i_counter))
print("number of 'o': {}".format(o_counter))
print("number of 'u': {}".format(u_counter))
