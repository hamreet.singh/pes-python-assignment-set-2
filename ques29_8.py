string = """lorem IPSUM olor SIT amet This is a lorem ipsum"""

print('Index of Substring I from the end : ', string.rfind('I'))
print('Index of Substring m from the end : ', string.rindex('m'))
print('Split String from right : ', string.rsplit(' '))
print('Split Lines : ', string.splitlines())
print('Startwith a substring : ', string.startswith('lorem'))
