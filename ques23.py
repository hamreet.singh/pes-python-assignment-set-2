import math

radius = float(input('Enter the radius of a circle : '))

area = math.pi * radius * radius

print('The area of the circle is : %.2f units' % area)
