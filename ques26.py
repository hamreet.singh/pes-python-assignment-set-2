encoded = "edvlf#fubswrjudskb"

print("encoded string: {}".format(encoded))
print('Algorithm: shift each character to left by 3 positions')

list_ = []

for i in encoded:
	if 32 <= ord(i) <= 47:
		list_.append(chr(ord(i) - 3))
	elif 48 <= ord(i) <= 50:
		list_.append(chr(10 + ord(i) - 3))
	elif 51 <= ord(i) <= 57:
		list_.append(chr(ord(i) - 3))
	elif ord(i)-3 < 97:
		list_.append(chr(26 + ord(i) - 3))
	else:
		list_.append(chr(ord(i) - 3))

decoded = "".join(list_)
print(decoded)
