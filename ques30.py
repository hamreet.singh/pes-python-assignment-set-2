N = int(input('Enter the value of N : '))

number_list = []

number_list = list(map(int, input(f'Enter {N} number(s) inline : ').split()))

print('Unsorted List : ', number_list)

for i in range(len(number_list)):

    for j in range(len(number_list)):

        if number_list[i] < number_list[j]:
            temp = number_list[i]
            number_list[i] = number_list[j]
            number_list[j] = temp

print('Sorted List : ', number_list)
